﻿using RateLimiting.API.RateLimiting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RateLimiting.API.Controllers
{
    public class ValuesController : ApiController
    {
        [HttpGet]
        [Route("api/rateLimiting")]
        [FilterAttribute]
        public async Task<HttpResponseMessage> Get(string key)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, new { 
                Result = "Ok", message = $"Get api successfully by key_Api: {key}" });
        }
    }
}
