﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace RateLimiting.API.RateLimiting
{
    public class FilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var keyApi = (string)actionContext.ActionArguments["key"] ?? string.Empty;
            var memoryCache = new MemoryCacher();
            var message = "Key Api is requeid!";
            if (keyApi.Length == 0)
            {
                actionContext.Response = actionContext.Request.CreateResponse((HttpStatusCode.BadRequest), message);
            }

            var allowExecute = false;
            var cacheResult = (CacheModel)memoryCache.GetValue(keyApi);
            if (cacheResult == null)
            {
                var model = new CacheModel
                {
                    Count = 1,
                    Start_time = DateTime.Now
                };
                memoryCache.Add(keyApi, model, DateTime.Now.AddMinutes(60));
                allowExecute = true;
            }
            else
            {
                if (DateTime.Now.AddMinutes(-1) > cacheResult.Start_time)
                {
                    cacheResult.Start_time = DateTime.Now;
                    cacheResult.Count = 1;
                }
                else if (cacheResult.Count > 3)
                {
                    allowExecute = false;
                }
                else
                {
                    cacheResult.Count++;
                    memoryCache.Delete(keyApi);
                    memoryCache.Add(keyApi, cacheResult, DateTime.Now.AddMinutes(60));
                    allowExecute = true;
                }
            }

            if (!allowExecute)
            {
                message = "You are only allowed to call 3requests /min.";
                actionContext.Response = actionContext.Request.CreateResponse((HttpStatusCode)429, message);
            }
        }
    }
}