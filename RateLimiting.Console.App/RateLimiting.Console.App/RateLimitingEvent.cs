﻿
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiting.App
{
    public class RateLimitingEvent
    {
        static HttpClient client = new HttpClient();
        public static async Task<string> GetInvocesIsReadyTest(string uri)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(uri);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
