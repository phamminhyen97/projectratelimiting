﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RateLimiting.App
{
    class Program
    {

        static async Task Main(string[] args)
        {
            string uriApi = ConfigurationManager.AppSettings["uriRateLimitingAPI"];
            while (true)
            {
                try
                {
                    var listString = new List<Task<string>>();

                    //call to api 15 times
                    for (int i = 0; i < 15; i++)
                    {
                        Console.WriteLine("Call Api number : " + i);
                        listString.Add(RateLimitingEvent.GetInvocesIsReadyTest(uriApi));
                    }
                    await Task.WhenAll(listString); //wait task done 
                    foreach (var item in listString) //write response api
                    {
                        Console.WriteLine(item.Result);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex}");
                }
                Thread.Sleep(30000);
            }

        }
    }
}
